# Quando um campo em um form está com esse, o Rails coloca uma div wrapper em
# volta da label e do field. Com esse código desabilitamos esse wrapper, pois
# ele causa bugs com o materialize.
ActionView::Base.field_error_proc = Proc.new do |html_tag, instance|
  html_tag.html_safe
end
