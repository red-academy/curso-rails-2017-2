Rails.application.routes.draw do
  root 'money_movements#index'

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  
  resources :categories
  resources :money_movements
  resources :users
  
  get 'profile/:id', to: 'users#show'

  namespace :api do
    resources :users, only: [:index, :create]
  end
end
