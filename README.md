# Bem vindo ao repositório da turma de Ruby on Rails 2017.2 da RedAcademy!

---

### Links importantes:
[Slack da RedAcademy](https://goo.gl/4gGn8p)

[Slide da aula 1](https://docs.google.com/presentation/d/1S0sfE-9XM0O4umvsCGED7LPfJdGeowcsY3Qr0Zvgle4/edit?usp=sharing) / [Formulário de satisfação da aula 1](https://goo.gl/forms/F3ZYu5MrFc5Ib6Sr1)

[Slide da Aula 2](https://goo.gl/8XrCQu) / [Formulário da aula 2](https://goo.gl/forms/cXR4ktAR27JHU2Pf1)

[Slide da aula 3](https://docs.google.com/presentation/d/1kyDrIzBXXpK8Jkt7J_5jqgw6ic7GVoH_6piQDNOJSZQ/edit?usp=sharing)

[Slide da aula 4](https://docs.google.com/presentation/d/1FwzJ-D0qFTchDwLS3ade3YQEcLEu0OlJEhz-V03wu64/edit?usp=sharing)

[Slide da aula 5](https://docs.google.com/presentation/d/10R4K8RTc-r9UZl9XUqHLevM6diPjwMKhJYv1OHQWeC4/edit?usp=sharing)

### Responsáveis pelo curso

* Caio Ergos - [GitHub](https://github.com/caioeps)
* Thiago Isaias - [GitHub](https://github.com/thiagoisaias)
* Guilherme Carneiro - [GitHub](https://github.com/GuiCarneiro)

### Aulas
Cada aula estará em uma branch.

Para baixar esse repositório:
```
git clone git@gitlab.com:red-academy/curso-rails-2017-2.git
# e então
cd curso-rails-2017-2
```

Para ver o código de uma aula específica:
```
git checkout -b nome-da-branch origin/nome-da-branch
```

### Iniciando o projeto
```
bundle install  # instalar as dependencias
rake db:create  # criar BD
rake db:migrate # migra as informações para o BD
rails s         # inicia o servidor
```

