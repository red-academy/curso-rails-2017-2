class CreateCategoryMoneyMovements < ActiveRecord::Migration[5.1]
  def change
    create_table :category_money_movements do |t|
      t.references :category, foreign_key: true
      t.references :money_movement, foreign_key: true

      t.timestamps
    end
  end
end
