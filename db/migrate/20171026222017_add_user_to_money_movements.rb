class AddUserToMoneyMovements < ActiveRecord::Migration[5.1]
  def change
    add_reference :money_movements, :user, foreign_key: true
  end
end
