class CreateMoneyMovementDescriptions < ActiveRecord::Migration[5.1]
  def change
    create_table :money_movement_descriptions do |t|
      t.text :body
      t.references :money_movement, foreign_key: true

      t.timestamps
    end
  end
end
