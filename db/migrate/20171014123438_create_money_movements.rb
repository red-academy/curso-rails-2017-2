class CreateMoneyMovements < ActiveRecord::Migration[5.1]
  def change
    create_table :money_movements do |t|
      t.float :amount

      t.timestamps
    end
  end
end
