module MenuHelper
    def menu_links
        if user_signed_in?
            [
              link_to('Categorias', categories_path),
              link_to(current_user.name, user_path(current_user)),
              link_to('Sair', destroy_user_session_path, method: :delete)
            ]
        else
            [
              link_to('Entrar', new_user_session_path)
            ]
        end
    end
end
