$(document).on('turbolinks:load', () => {
  $('input.money').maskMoney({
    decimal: ',',
    prefix: 'R$ ',
    thousands: '.'
  });

  // Function to apply mask on load.
  $('input.money').each(function() {
    $(this).maskMoney('mask', $(this).val());
  })
})


