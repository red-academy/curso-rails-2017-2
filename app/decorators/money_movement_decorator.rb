class MoneyMovementDecorator < ApplicationDecorator
  delegate_all

  def amount
    h.number_to_currency(object.amount)
  end

  def categories_names_separated_by_comma
    object.categories.map(&:name).sort.join(', ')
  end

  def created_at
    h.l object.created_at.to_date, format: :long
  end
end
