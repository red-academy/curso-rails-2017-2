class UserDecorator < ApplicationDecorator
  delegate_all

  def name
    object.name.split(' ').first
  end
  
  def full_name
    object.name
  end
end
