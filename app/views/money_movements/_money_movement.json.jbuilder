json.extract! money_movement, :id, :amount, :created_at, :updated_at
json.url money_movement_url(money_movement, format: :json)
