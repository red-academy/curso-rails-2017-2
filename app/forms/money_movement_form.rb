class MoneyMovementForm < Reform::Form
  property :amount
  property :category_ids
  property :user

  property :description do
    property :body
    validates :body, length: { maximum: 50 }
  end

  validates :amount,
            presence: true,
            format: { with: /(R\$\s?)(\d|\.)*,(\d{2})/ }

  validates :user,
            presence: true

  def initialize(money_movement)
    money_movement.build_description unless money_movement.description
    super money_movement
  end

  def save
    normalize_amount
    super
  end

  private

  def normalize_amount
    self.amount.gsub!(/R\$|\s|\./, '').gsub!(',', '.')
  end
end
