class CategoryForm < Reform::Form
  property :name

  validates :name, presence: true

  validate :name_must_be_1_word_length

  private

  def name_must_be_1_word_length
    if name.split(' ').length > 1
      errors.add(:name, 'too big! Must be 1 word length')
    end
  end
end
