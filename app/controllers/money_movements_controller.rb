class MoneyMovementsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_money_movement, only: [:show, :edit, :update, :destroy]

  def index
    @money_movements =
      MoneyMovementDecorator.decorate_collection(current_user
                                                   .money_movements
                                                   .order(created_at: :desc)
                                                   .includes(:categories))
  end

  def show
  end

  def new
    @money_movement_form = MoneyMovementForm.new(MoneyMovement.new)
  end

  def edit
    @money_movement_form = MoneyMovementForm.new(@money_movement)
  end

  def create
    @money_movement_form =
      MoneyMovementForm.new(current_user.money_movements.build)

    if @money_movement_form.validate(money_movement_params)
      @money_movement_form.save
      redirect_to money_movements_path, notice: 'Money movement was successfully created.'
    else
      render :new
    end
  end

  def update
    @money_movement_form = MoneyMovementForm.new(@money_movement)

    if @money_movement_form.validate(money_movement_params)
      @money_movement_form.save
      redirect_to money_movements_path, notice: 'Money movement was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @money_movement.destroy
    redirect_to money_movements_url, notice: 'Money movement was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_money_movement
    @money_movement = MoneyMovement.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def money_movement_params
    params.require(:money_movement).permit(
      :amount,
      category_ids: [],
      description_attributes: [:body]
    )
  end
end
