class MoneyMovement < ApplicationRecord
  belongs_to :user

  has_many :category_money_movements, dependent: :destroy
  has_many :categories, through: :category_money_movements

  has_one :description, class_name: 'MoneyMovementDescription', dependent: :destroy

  validates_presence_of :amount
end
