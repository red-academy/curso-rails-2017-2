class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
  has_many :categories
  has_many :money_movements
  
  validates_presence_of :name
  
  validates_length_of :username, minimum: 3, maximum: 20
  
  before_validation :generate_username
  
  private

  def generate_username
    if username.blank?
      self.username = self.name + SecureRandom.random_number(100000).to_s
    end
  end
end
