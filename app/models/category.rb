class Category < ApplicationRecord
  belongs_to :user
  has_many :category_money_movements
  has_many :money_movements, through: :category_money_movements
end
