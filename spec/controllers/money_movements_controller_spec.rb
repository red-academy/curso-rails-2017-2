require 'rails_helper'

RSpec.describe MoneyMovementsController, type: :controller do
  describe 'GET #index' do
    subject { get :index }

    context 'when user is signed in' do
      before :each do
        user = User.create!(
          email: 'mat@email.com',
          name: 'Matheus',
          password: '123456',
          password_confirmation: '123456')

        sign_in user
      end

      it 'return http status 200' do
        subject
        expect(response).to have_http_status(200)
      end

      it 'assigns money_movements' do
        subject
        expect(assigns(:money_movements)).not_to eq(nil)
      end
    end

    context 'when there is no user signed in' do
      it 'return http status 302' do
        subject
        expect(response).to have_http_status(302)
      end

      it 'redirects to log in page' do
        subject
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end
end
