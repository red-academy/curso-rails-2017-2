require 'rails_helper'

RSpec.describe MenuHelper, type: :helper do
  let(:user) { create :user }

  describe '#menu_links' do
    context 'when there is a user signed in' do
      before :each do
        sign_in user
      end

      it 'have a logout link' do
        expect(helper.menu_links.join).to match(/Sair/)
      end
    end

    context 'when there is no user signed in' do
      it 'have a logout link' do
        expect(helper.menu_links.join).to match(/Entrar/)
      end
    end
  end
end
