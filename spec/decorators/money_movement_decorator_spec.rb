require 'rails_helper'

RSpec.describe MoneyMovementDecorator, type: :decorator do
  let(:money_movement) { build_stubbed(:money_movement, :expensive) }

  let(:decorator) { described_class.new(money_movement) }

  describe '#amount' do
    subject { decorator.amount }

    it 'show the amount as currency' do
      expect(subject).to match(/(R\$\s.*)/)
    end
  end
end
