FactoryBot.define do
  factory :money_movement do
    amount 11.11

    user

    trait :expensive do
      amount 10_000.00
    end
  end
end
