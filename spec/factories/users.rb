FactoryBot.define do
  factory :user do
    name 'Adonias'
    sequence(:email) { |i| 'email#{i}@email.com' }
    password '123456'
    password_confirmation '123456'
  end
end
